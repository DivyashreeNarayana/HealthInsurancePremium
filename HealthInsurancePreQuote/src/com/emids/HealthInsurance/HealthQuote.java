package com.emids.HealthInsurance;

import java.util.Map;
import java.util.Map.Entry;

public class HealthQuote implements InsuranceQuote {

	@Override
	public float CalculateQuote(float amount) {
		
		return  getHealthPremium(amount,Premium.getPreconditions());
	}

	public float getHealthPremium(float basepremium, Map<String, String> preconditions) {
		float preconditionsResult = 0;
		for (Entry<String, String> entry : preconditions.entrySet()) {
			if ((entry.getKey().equals("Hypertension") || entry.getKey().equals("BloodPressure")
					|| entry.getKey().equals("BloodSugar") || entry.getKey().equals("Overweight"))
					&& entry.getValue().equals("Yes")) {
				preconditionsResult = preconditionsResult + ((basepremium * 1f) / 100f);
			}
		}
		return  preconditionsResult;
	}
}
