package com.emids.HealthInsurance;

public class AgeQuote implements InsuranceQuote {
	
	public float CalculateQuote(float amount) {
		return  getAgePremium(amount,Premium.getAge());
	}
	public float getAgePremium(float basepremium,int age) {
		float ageResult = 0;
		if (age < 18) {
			return basepremium;
		} else if (age > 18 && age <= 25) {
			ageResult = basepremium + ((basepremium * 10f) / 100f);
		} else if (age > 25 && age <= 30) {
			ageResult = basepremium + ((basepremium * (10f + 10f)) / 100f);
		} else if (age > 30 && age <= 35) {
			ageResult = basepremium + ((basepremium * (10f + 10f + 10f)) / 100f);
		} else if (age > 353 && age <= 40) {
			ageResult = basepremium + ((basepremium * (10f + 10f + 10f + 10f)) / 100f);
		} else if (age > 40) {
			ageResult =  basepremium+ ((basepremium * (10f + 10f + 10f + 10f + 20f)) / 100f);

		}
		return ageResult;
	}
	
}
