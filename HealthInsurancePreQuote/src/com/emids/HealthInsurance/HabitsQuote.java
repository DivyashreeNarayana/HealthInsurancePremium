package com.emids.HealthInsurance;

import java.util.Map;
import java.util.Map.Entry;

public class HabitsQuote implements InsuranceQuote{

	@Override
	public float CalculateQuote(float amount) {
		
		return getHabitsPremium(amount,Premium.getHabits());
	}

	public float getHabitsPremium(float basepremium,Map<String, String> habits) {
		float habitsResult = 0;
		for (Entry<String, String> entry : habits.entrySet()) {
			if ((entry.getKey().equals("Smoking") || entry.getKey().equals("Alcohol")
					|| entry.getKey().equals("Drugs") )
					&& entry.getValue().equals("Yes")) {
				habitsResult = habitsResult + ((basepremium * 3f) / 100f);
			}
			else if(entry.getKey().equals("DailyExercise")&& entry.getValue().equals("Yes")){
				habitsResult = habitsResult - ((basepremium * 3f) / 100f);
			}
		}
		return  habitsResult;
	}
	
}
