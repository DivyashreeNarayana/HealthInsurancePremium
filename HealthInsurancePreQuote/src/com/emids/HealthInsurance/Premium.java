package com.emids.HealthInsurance;

import java.util.HashMap;
import java.util.Map;

public class Premium {
	private static float basepremium=5000f;
	private static String name ;
	private static int age ;
	private static String gender;
	private static String family;


	private static Map<String, String> preconditions = new HashMap<String, String>();
	private static Map<String, String> habits = new HashMap<String, String>();
	
	
	public  Premium(int age, String gender, Map<String, String> preconditions, Map<String, String> habits, String family) {
		Premium.age=age;
		Premium.gender=gender;
		Premium.preconditions=preconditions;
		Premium.habits=habits;
		Premium.family=family;
		
	}
	public static String getFamily() {
		return family;
	}
	public static float getBasepremium() {
		return basepremium;
	}
	public void setBasepremium(float basepremium) {
		Premium.basepremium = basepremium;
	}
	public static String getName() {
		return name;
	}

	public static int getAge() {
		return age;
	}


	public static String getGender() {
		return gender;
	}

	

	public static Map<String, String> getPreconditions() {
		return preconditions;
	}


	public static Map<String, String> getHabits() {
		return habits;
	}
}