package com.emids.HealthInsurance.Test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.emids.HealthInsurance.HabitsQuote;

public class HabitsQuoteTest {
	Map<String, String> habits = new HashMap<String, String>();
	

	@Test
	public final void testGetHabitsPremium() {
		
		HabitsQuote test = new HabitsQuote();
		habits.put("Smoking", "No");
		habits.put("Alcohol", "Yes");
		habits.put("DailyExercise", "Yes");
		habits.put("Drugs", "No");
		float basepremium=5000f;
		float actual =basepremium*(3f-3f)/100;
		float result =  test.getHabitsPremium(basepremium, habits);
		assertNotNull(result);
		assertTrue (actual == result);
		
		
	}

}
