package com.emids.HealthInsurance.Test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.emids.HealthInsurance.HealthQuote;

public class HealthQuoteTest {
	Map<String, String> preconditions = new HashMap<String, String>();
	@Test
	public final void testGetHealthPremium() {
		HealthQuote test = new HealthQuote();
		preconditions.put("Hypertension", "No");
		preconditions.put("BloodPressure", "No");
		preconditions.put("BloodSugar", "No");
		preconditions.put("Overweight", "Yes");
		float basepremium=5000f;
		float actual =basepremium*1f/100;
	
		float result =  test.getHealthPremium(basepremium,preconditions);
		
		assertNotNull(result);
		assertTrue (actual == result);

	}

}
