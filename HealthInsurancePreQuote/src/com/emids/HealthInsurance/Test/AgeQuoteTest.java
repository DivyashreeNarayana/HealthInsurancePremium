package com.emids.HealthInsurance.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.HealthInsurance.AgeQuote;

public class AgeQuoteTest {

	@Test
	public void testGetAgePremium() {
		AgeQuote test = new AgeQuote();
		float basepremium=5000f;
		float actual =basepremium*30f/100;
		float result =  test.getAgePremium(basepremium, 34);
		assertNotNull(result);
		assertTrue (actual == result);
	}

}
