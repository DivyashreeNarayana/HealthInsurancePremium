package com.emids.HealthInsurance.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.HealthInsurance.GenderQuote;

public class GenderQuoteTest {

	@Test
	public final void testGetGenderPremium() {
		GenderQuote test = new GenderQuote();
		float basepremium=5000f;
		float actual =basepremium*2f/100;
		
		float result =  test.getGenderPremium(basepremium, "Male");
		assertNotNull(result);
		assertTrue (actual == result);

	}

}
