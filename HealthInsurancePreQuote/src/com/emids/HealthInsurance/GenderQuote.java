package com.emids.HealthInsurance;

public class GenderQuote implements InsuranceQuote {

	@Override
	public float CalculateQuote(float amount) {
		return getGenderPremium(Premium.getBasepremium(),Premium.getGender());
	}

	public float getGenderPremium(float basepremium,String gender) {
		float genderResult = 0;
		if (gender.equals("Male")) {
			genderResult =((basepremium * 2f) / 100f);
		}
		return  genderResult;

	}
	

}
