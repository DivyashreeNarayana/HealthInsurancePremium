package com.emids.HealthInsurance;


	public interface InsuranceQuote {

		public float CalculateQuote(float amount);
	}

