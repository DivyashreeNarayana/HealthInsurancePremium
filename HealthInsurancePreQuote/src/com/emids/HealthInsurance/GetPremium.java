package com.emids.HealthInsurance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GetPremium {
	
	  private InsuranceQuote strategy;

	  //this can be set at runtime by the application preferences

	  public void setInsuranceQuote(InsuranceQuote strategy) {

	    this.strategy = strategy;

	  }

	  //use the strategy

	public float CacltulateTotalPremium(Map<String,InsuranceQuote> quote) {
		
		float sum=0;
		for (Map.Entry<String,InsuranceQuote > entry : quote.entrySet()) {
			setInsuranceQuote(entry.getValue());
			sum+=strategy.CalculateQuote(Premium.getBasepremium());
			
		}

		
		
		
		return sum;
	}
	
}
