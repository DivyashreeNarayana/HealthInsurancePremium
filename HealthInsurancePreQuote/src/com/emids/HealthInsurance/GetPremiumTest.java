package com.emids.HealthInsurance;

import java.util.HashMap;
import java.util.Map;

public class GetPremiumTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int age = 34;
		String gender = "Male";
		String family = "Yes";
		Map<String, String> preconditions = new HashMap<String, String>();
		preconditions.put("Hypertension", "No");
		preconditions.put("BloodPressure", "No");
		preconditions.put("BloodSugar", "No");
		preconditions.put("Overweight", "Yes");
		Map<String, String> habits = new HashMap<String, String>();
		habits.put("Smoking", "No");
		habits.put("Alcohol", "Yes");
		habits.put("DailyExercise", "Yes");
		habits.put("Drugs", "No");
		Premium premium = new Premium(age, gender, preconditions, habits, family);
		GetPremium test = new GetPremium();
		StratergyContext strategyContext=new StratergyContext();
		
		float Total = test.CacltulateTotalPremium(strategyContext.strategyContext);
				
		System.out.println("Health Insurance Premium for Mr. Gomes: Rs. " + Total);

	}

}
